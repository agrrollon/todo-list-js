
let tasks = [];
let todoFormBox = document.getElementById('todoForm');
let todoListItems = document.getElementById('todoList');
let todoInputList = document.getElementById('todoInput');
let addItemList = document.getElementById('addItem');
let clearItemList = document.getElementById('clearItem');
let messagePrompt = document.getElementById('message');
let todosDoneList = document.getElementById('todosDone');
let firstItemList = document.getElementById('firstItem');
let lastItemList = document.getElementById('lastItem');

addItemList.addEventListener('click', addItem);

function addItem(event) {
    if (todoInputList.value == "") {
        event.preventDefault();
        alert("Please Enter Task!");
        return "";
    }
    let todoInputListItem = todoInputList.value;
    tasks.push(todoInputListItem);

   
    let ol = document.getElementById('todoList');
    let list = document.getElementById('todoInput');
    let li = document.createElement('li');
    li.setAttribute('id', list.value);
    li.appendChild(document.createTextNode(list.value));
    ol.appendChild(li);


    if (tasks.length > 10) {
        messagePrompt.innerHTML = "You are Awesome! Great Job";
    } else if (tasks.length == 10) {
        messagePrompt.innerHTML = "Very good! You made it!";
    } else if (tasks.length >= 5 && tasks.length < 10) {
        messagePrompt.innerHTML = "You are good!";
    } else {
        messagePrompt.innerHTML = "Please Improve your work next time";
    }

    lastItemList.innerHTML = tasks[tasks.length - 1];
    firstItemList.innerHTML = tasks[0];
    todosDoneList.innerHTML = tasks.length;

}

clearItemList.addEventListener('click', clearItem);

function clearItem() {
    let ol = document.getElementById('todoList');
    ol.remove(ol);
    tasks = [];
    todoInputList = '';
   
    lastItemList.innerHTML = "";
    firstItemList.innerHTML = "";
    todosDoneList.innerHTML = "";
    message.innerHTML = '';

}